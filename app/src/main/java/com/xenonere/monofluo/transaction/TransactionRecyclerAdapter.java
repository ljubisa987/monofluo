package com.xenonere.monofluo.transaction;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.model.Transaction;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TransactionRecyclerAdapter
        extends RecyclerView.Adapter<TransactionRecyclerAdapter.ViewHolder> {

    private final View.OnClickListener mOnClickListener;

    private List<Transaction> mTransactionList;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.amount_textView)
        public TextView mAmount;
        @Bind(R.id.date_textView)
        public TextView mDate;
        @Bind(R.id.description_textView)
        public TextView mDescription;
        @Bind(R.id.typeIndicator_imageView)
        public ImageView mIndicator;
        @Bind(R.id.ownerIndicator_imageView)
        public ImageView mOwnerIndicator;
        @Bind(R.id.accountIndicator_imageView)
        public ImageView mAccountIndicator;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public TransactionRecyclerAdapter(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
        mTransactionList = new ArrayList<>(0);
    }

    public void setTransactionList(List<Transaction> transactionList) {
        mTransactionList = transactionList;
        notifyDataSetChanged();
    }

    public void clearList() {
        mTransactionList.clear();
        notifyDataSetChanged();
    }


    public List<Transaction> getTransactionList() {
        return mTransactionList;
    }

    @Override
    public TransactionRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
            int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_list_item, parent, false);
        v.setOnClickListener(mOnClickListener);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Transaction transaction = mTransactionList.get(position);

        Drawable calendarIcon = holder.mDate.getCompoundDrawables()[0];

        Resources resources = holder.mAmount.getResources();
        if (calendarIcon != null) {
            int tintColor = resources.getColor(R.color.textColorSecondary);
            DrawableCompat.setTint(calendarIcon, tintColor);
        }
        boolean isCurrentUserTransactionOwner = ParseHelper.isCurrentUser(transaction.getOwner());
        boolean isCurrentUserAccountOwner = ParseHelper.isCurrentUser(transaction.getAccount().getOwner());

        Drawable ownerIndicator = ContextCompat.getDrawable(holder.mOwnerIndicator.getContext(),
                isCurrentUserTransactionOwner ? R.drawable.ic_person_black_24dp : R.drawable.ic_person_outline_black_24dp);
        Drawable accountIndicator = ContextCompat.getDrawable(holder.mAccountIndicator.getContext(),
                isCurrentUserAccountOwner ? R.drawable.ic_folder_black_24dp : R.drawable.ic_folder_shared_black_24dp);

        DrawableCompat.setTint(holder.mIndicator.getDrawable(), resources.getColor(R.color.amountSpendingColor));
        DrawableCompat.setTint(ownerIndicator, resources.getColor(R.color.textColorSecondary));
        DrawableCompat.setTint(accountIndicator, resources.getColor(R.color.textColorSecondary));

        holder.mOwnerIndicator.setImageDrawable(ownerIndicator);
        holder.mAccountIndicator.setImageDrawable(accountIndicator);

        holder.mAmount.setText(transaction.getFormattedAmount());
        holder.mDescription.setText(transaction.getDescription());
        holder.mDate.setText(transaction.getFormatedDate());
    }

    @Override
    public int getItemCount() {
        return mTransactionList.size();
    }
}