package com.xenonere.monofluo.transaction;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.account.AccountDetailsActivity;
import com.xenonere.monofluo.account.AccountListAdapter;
import com.xenonere.monofluo.common.BaseFragment;
import com.xenonere.monofluo.common.DatePickerFragment;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.model.Transaction;
import com.xenonere.monofluo.util.CalendarUtil;
import com.xenonere.monofluo.util.DialogHelper;
import com.xenonere.monofluo.util.KeyboardUtil;
import com.xenonere.monofluo.util.PreferenceManager;
import com.xenonere.monofluo.util.ViewHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;


// TODO: transaciton not visible after user account shared from account owner
public class TransactionDetailFragment extends BaseFragment
        implements DatePickerFragment.Listener, AdapterView.OnItemSelectedListener {

    @Bind(R.id.account_spinner)
    Spinner mAccountSpinner;
    @Bind(R.id.date_button)
    Button mDateButton;
    @Bind(R.id.amount_editText)
    EditText mAmountEditText;
    @Bind(R.id.comment_editText)
    EditText mCommentEditText;
    @Bind(R.id.description_editText)
    EditText mDescriptionEditText;
    @Bind(R.id.currency_textView)
    TextView mCurrencyTextView;
    @Bind(R.id.rootLayout)
    ViewGroup mRootLayout;
    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;


    @Bind({R.id.account_spinner, R.id.date_button, R.id.amount_editText,
            R.id.comment_editText, R.id.description_editText})
    List<View> mAllFormViews;

    private List<Account> mAccountsList;
    private Transaction mTransaction;
    private AccountListAdapter mAccountListAdapter;

    private String mTransactionObjectId;
    private Calendar mTransactionDate;
    private boolean mStateRestored;

    @Override
    protected int getContentView() {
        return R.layout.fragment_transaction_detail;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            mStateRestored = true;
            mTransactionObjectId = savedInstanceState.getString("mTransactionObjectId");
            mTransactionDate = (Calendar) savedInstanceState.getSerializable("mTransactionDate");
        }

        setupActivityAppBar(R.drawable.ic_close_white_24dp);
        mToolbar.setTitle(R.string.info_transaction_spending);
        setHasOptionsMenu(true);
        mAccountListAdapter = new AccountListAdapter(getActivity(), false);
        mAccountSpinner.setAdapter(mAccountListAdapter);
        mCurrencyTextView.setText(Transaction.getCurrency());
        loadAccounts();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mTransactionObjectId", mTransactionObjectId);
        outState.putSerializable("mTransactionDate", mTransactionDate);
    }

    private void loadTransaction() {
        ParseQuery<Transaction> query = Transaction.getQuery();
        query.include(Transaction.KEY_ACCOUNT);
        query.include(Transaction.KEY_OWNER);
        query.whereEqualTo(Transaction.KEY_OBJECT_ID, mTransactionObjectId);
        query.getFirstInBackground(new GetCallback<Transaction>() {
            @Override
            public void done(Transaction transaction, ParseException e) {
                if (isVisible()) {
                    if (e == null) {
                        mTransaction = transaction;
                        if (!mStateRestored) {
                            populateFromTransaction();
                        }
                        onDataLoadingFinished();
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                        DialogHelper.showError(e, getActivity(), getFragmentManager(), new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                getActivity().onNavigateUp();
                            }
                        });
                    }
                }
            }
        });
    }

    public void editTransaction(String uuid) {
        mTransactionObjectId = uuid;
        if (mAccountsList != null) {
            loadTransaction();
        }
    }

    private void populateFromTransaction() {
        mAmountEditText.setText(mTransaction.getAmount());
        mDescriptionEditText.setText(mTransaction.getDescription());
        mCommentEditText.setText(mTransaction.getComment());
        setSelectedAccount(mTransaction.getAccount().getObjectId());
    }

    public void newTransaction() {
        if (mAccountsList != null) {
            mTransaction = Transaction.createNew();
        }
    }

    private void loadAccounts() {
        ParseQuery<Account> userAccount = Account.getQuery();
        userAccount.whereEqualTo(Account.KEY_OWNER, ParseUser.getCurrentUser());
        ParseQuery<Account> sharedAccount = Account.getQuery();
        sharedAccount.whereEqualTo(Account.KEY_SHARE_LIST, ParseUser.getCurrentUser());
        List<ParseQuery<Account>> queries = new ArrayList<>();

        queries.add(userAccount);
        queries.add(sharedAccount);
        ParseQuery<Account> mainQuery = ParseQuery.or(queries);
        mainQuery.findInBackground(new FindCallback<Account>() {
            @Override
            public void done(List<Account> objects, ParseException e) {
                mAccountsList = objects;
                onAccountsFetched();
            }
        });
    }

    private void onAccountsFetched() {
        if (mAccountsList == null || mAccountsList.isEmpty()) {
            mProgressBar.setVisibility(View.GONE);
            DialogHelper.showSnackbarkMessage(getActivity(), getView(),
                    R.string.error_no_account_no_transaction_message,
                    R.string.action_create_account,
                    Snackbar.LENGTH_INDEFINITE, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getActivity().onNavigateUp();
                            AccountDetailsActivity.createAccount(getActivity());
                        }
                    });
            return;
        }

        if (mTransactionObjectId != null) {
            loadTransaction();
        } else {
            mTransaction = Transaction.createNew();
            onDataLoadingFinished();
        }

    }

    private void onDataLoadingFinished() {
        if (mTransactionDate == null) {
            mTransactionDate = mTransaction.getTransactionDateCalendar();
        }
        populateTransactionDate(mTransactionDate);
        if (!ParseHelper.isCurrentUser(mTransaction.getOwner())) {
            mToolbar.setSubtitle(mTransaction.getOwner().getUsername());
        }
        mAccountSpinner.setOnItemSelectedListener(this);
        mAmountEditText.requestFocus();
        populateAccountChooser();
        getActivity().invalidateOptionsMenu();
        if (doesUserHaveWriteAccess()) {
            setWidgetsEditable(true);
            mAmountEditText.requestFocus();
            KeyboardUtil.showKeyboard(mAmountEditText);
        } else {
            setWidgetsEditable(false);
        }
        mRootLayout.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    private void populateAccountChooser() {
        mAccountListAdapter.clear();
        mAccountListAdapter.addAll(mAccountsList);
        String accountId;
        if (mTransaction.getAccount() != null && mTransaction.getAccount().getObjectId() != null) {
            accountId = mTransaction.getAccount().getObjectId();
        } else {
            accountId = PreferenceManager.getInstance().getDefaultTransactionAccount();
        }
        if (!TextUtils.isEmpty(accountId)) {
            setSelectedAccount(accountId);
        }
    }

    private void setWidgetsEditable(boolean isEditable) {
        ButterKnife.apply(mAllFormViews, ViewHelper.ENABLED, isEditable);
    }

    private void setSelectedAccount(String accountObjectId) {
        for (int i = 0; i < mAccountsList.size(); i++) {
            if (accountObjectId.equals(mAccountsList.get(i).getObjectId())) {
                mAccountSpinner.setSelection(i, true);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.transaction_details_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_save).setVisible(canSaveTransaction());
        menu.findItem(R.id.menu_remove).setVisible(canRemoveTransaction());
    }

    private boolean canSaveTransaction() {
        return mTransaction != null && doesUserHaveWriteAccess() &&
                !TextUtils.isEmpty(mAmountEditText.getText());
    }

    private boolean canRemoveTransaction() {
        return mTransactionObjectId != null && mTransaction != null &&
                doesUserHaveWriteAccess();
    }

    private boolean doesUserHaveWriteAccess() {
        return mTransaction.getACL().getWriteAccess(ParseUser.getCurrentUser());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                saveTransaction();
                return true;
            case R.id.menu_remove:
                removeTransaction();
                return true;
        }
        return false;
    }

    private void removeTransaction() {
        mTransaction.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                Account account = mTransaction.getAccount();
                if (account.isShared()) {
                    ParseHelper.notifyAccountUpdate(account);
                }
                if (isVisible()) {
                    if (e == null) {
                        finishWithResult();
                        Toast.makeText(getActivity(), "Transaction removed!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        DialogHelper.showError(e, getActivity(), getFragmentManager(), new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finishWithResult();
                            }
                        });
                    }
                }
            }
        });
    }

    private void saveTransaction() {
        final Account account = mAccountsList.get(mAccountSpinner.getSelectedItemPosition());
        mTransaction.setAmount(mAmountEditText.getText().toString());
        mTransaction.setAccount(account);
        mTransaction.setDescription(mDescriptionEditText.getText().toString());
        mTransaction.setComment(mCommentEditText.getText().toString());
        mTransaction.setTransactionDate(mTransactionDate.getTimeInMillis());
        mTransaction.invalidateAcl();
        mTransaction.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (account.isShared()) {
                    ParseHelper.notifyAccountUpdate(account);
                }
                if (!isDetached()) {
                    if (e == null) {
                        Toast.makeText(getActivity(), "Transaction saved!",
                                Toast.LENGTH_SHORT).show();
                        finishWithResult();
                    } else if (isVisible()) {
                        DialogHelper.showError(e, getActivity(), getFragmentManager(),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        finishWithResult();
                                    }
                                });
                    }
                }
            }
        });
    }

    private void finishWithResult() {
        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

    @OnClick(R.id.date_button)
    void openDatePicker() {
        DatePickerFragment datePickerFragment = DatePickerFragment
                .newInstance(R.string.title_pick_date);
        datePickerFragment.show(getFragmentManager(), DatePickerFragment.TAG);
        datePickerFragment.setListener(this);
    }

    @OnTextChanged(R.id.amount_editText)
    void amountChanged(CharSequence amount) {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onDatePickerDone(Calendar selectedDate) {
        mTransactionDate = selectedDate;
        populateTransactionDate(selectedDate);

    }

    private void populateTransactionDate(Calendar transactionDate) {
        if (CalendarUtil.isToday(transactionDate)) {
            mDateButton.setText(getString(R.string.info_today));
        } else {
            String formattedDate = CalendarUtil.DATE_FORMAT_D_M_Y.format(transactionDate.getTime());
            mDateButton.setText(formattedDate);
        }
    }

    @Override
    public void onDatePickerCanceled() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        PreferenceManager.getInstance().saveDefaultTransactionAccount(mAccountsList.get(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
