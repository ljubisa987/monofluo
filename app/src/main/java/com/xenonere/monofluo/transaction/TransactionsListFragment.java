package com.xenonere.monofluo.transaction;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.account.AccountFilterDialog;
import com.xenonere.monofluo.common.RecyclerViewFragment;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.model.Transaction;
import com.xenonere.monofluo.print.PrintTask;
import com.xenonere.monofluo.util.DialogHelper;
import com.xenonere.monofluo.util.PreferenceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TransactionsListFragment extends RecyclerViewFragment<TransactionRecyclerAdapter>
        implements View.OnClickListener, AccountFilterDialog.Listener {

    public static final String TAG = TransactionsListFragment.class.toString();
    private PrintTask mPrintTask;

    public static TransactionsListFragment newInstance() {
        return new TransactionsListFragment();
    }

    private List<Account> mAccountsList;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        fetchAccounts();
    }

    @Override
    protected int getNoContentText() {
        return R.string.info_no_transactions;
    }

    @Override
    protected TransactionRecyclerAdapter onAdapterCreate() {
        return new TransactionRecyclerAdapter(this);
    }

    @Override
    protected boolean hasAddButton() {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.transaction_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_print_transaction:
                if (getAdapter().getItemCount() > 0) {
                    if (mPrintTask != null) {
                        mPrintTask.cancel(true);
                    }
                    mPrintTask = new PrintTask(getActivity());
                    mPrintTask.execute();
                } else {
                    Toast.makeText(getActivity(), R.string.info_no_transacation_for_printing,
                            Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.menu_filter_transaction:
                AccountFilterDialog accountFilterDialog = AccountFilterDialog.newInstance();
                accountFilterDialog.show(getFragmentManager(), AccountFilterDialog.TAG);
                accountFilterDialog.setListener(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void refreshContent() {
        fetchAccounts();
    }

    private void loadTransactions() {
        if (mAccountsList == null || mAccountsList.isEmpty()) {
            getAdapter().clearList();
            showNoContent(true);
            updateLoadingView(false);
            return;
        }

        ParseQuery<Transaction> query = ParseQuery.getQuery(Transaction.class);
        Set<String> excludedAccounts = PreferenceManager.getInstance().getExcludedAccounts();
        List<Account> accountList = new ArrayList<>(excludedAccounts.size());
        for (Account account : mAccountsList) {
            if (!excludedAccounts.contains(account.getObjectId())) {
                accountList.add(account);
            }
        }
        query.whereContainedIn(Transaction.KEY_ACCOUNT, accountList);
        query.addDescendingOrder(Transaction.KEY_TRANSACTION_DATE);
        query.include("account.owner");
        query.findInBackground(new FindCallback<Transaction>() {
            @Override
            public void done(List<Transaction> transactions, ParseException e) {
                updateLoadingView(false);
                if (e == null) {
                    getAdapter().setTransactionList(transactions);
                    showNoContent(transactions.isEmpty());
                } else {
                    if (isVisible()) {
                        DialogHelper.showError(e, getActivity(),
                                getActivity().getSupportFragmentManager());
                    }
                }
            }
        });
    }

    private void fetchAccounts() {
        updateLoadingView(true);
        ParseQuery<Account> userAccount = Account.getQuery();
        userAccount.whereEqualTo(Account.KEY_OWNER, ParseUser.getCurrentUser());
        ParseQuery<Account> sharedAccount = Account.getQuery();
        sharedAccount.whereEqualTo(Account.KEY_SHARE_LIST, ParseUser.getCurrentUser());
        List<ParseQuery<Account>> queries = new ArrayList<>();
        queries.add(userAccount);
        queries.add(sharedAccount);
        ParseQuery<Account> mainQuery = ParseQuery.or(queries);
        mainQuery.findInBackground(new FindCallback<Account>() {
            @Override
            public void done(List<Account> accountList, ParseException e) {
                mAccountsList = accountList;
                if (!isVisible()) {
                    updateLoadingView(false);
                    return;
                }

                loadTransactions();

                if (e != null) {
                    DialogHelper.showError(e, getActivity(), getFragmentManager());
                } else if (accountList.isEmpty() && !PreferenceManager.getInstance()
                        .isDefaultAccountCreated()) {
                    final Account defaultAccount = Account.createNew();
                    defaultAccount.setTitle(Account.generateDefAccountName((String) ParseUser.
                            getCurrentUser().get("name")));
                    defaultAccount.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                PreferenceManager.getInstance().setDefaultAccountCreated();
                                if (!isRemoving()) {
                                    DialogHelper.showSnackbarkMessage(getActivity(), getView(),
                                            R.string.info_default_account_created,
                                            R.string.action_ok,
                                            Snackbar.LENGTH_INDEFINITE, null);
                                }
                            }

                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onAddButtonClick() {
        TransactionDetailActivity.createTransaction(getActivity());
    }

    @Override
    public void onClick(View v) {
        Transaction transaction = getAdapter().getTransactionList().get(
                mRecyclerView.getChildAdapterPosition(v));
        TransactionDetailActivity.showTransactionDetails(getActivity(), transaction.getObjectId());

    }

    @Override
    public void onFilterChanged() {
        loadTransactions();
    }
}
