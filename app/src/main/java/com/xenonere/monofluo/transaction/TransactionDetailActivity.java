package com.xenonere.monofluo.transaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.util.Contract;

public class TransactionDetailActivity extends BaseActivity {

    public static final String EXTRA_OBJECT_ID = "extraObjectId";

    public static void createTransaction(Activity activity) {
        Intent intent = new Intent(activity, TransactionDetailActivity.class);
        intent.setAction(Intent.ACTION_INSERT);
        activity.startActivityForResult(intent, 10);
    }

    public static void showTransactionDetails(Activity activity, String objectId) {
        Intent intent = new Intent(activity, TransactionDetailActivity.class);
        intent.putExtra(EXTRA_OBJECT_ID, objectId);
        intent.setAction(Intent.ACTION_VIEW);
        activity.startActivityForResult(intent, 11);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_transaction_detail;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState == null) {
            TransactionDetailFragment fragment = (TransactionDetailFragment)
                    getSupportFragmentManager().findFragmentById(R.id.transactionDetailFragment);
            if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
                String objectId = getIntent().getStringExtra(EXTRA_OBJECT_ID);
                Contract.isNotNull(objectId);
                fragment.editTransaction(objectId);
            } else {
                fragment.newTransaction();
            }
        }
    }
}
