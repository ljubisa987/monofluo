package com.xenonere.monofluo.push;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class PushBroadcastReceiver extends com.parse.ParsePushBroadcastReceiver {

    public static final String ACTION_REFRESH = "actionRefresh";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        JSONObject pushData = null;
        try {
            pushData = new JSONObject(intent.getStringExtra(KEY_PUSH_DATA));
        } catch (JSONException e) {
            Log.e("broadcast", "Unexpected JSONException when receiving push data: ", e);
        }

        String action = null;
        if (pushData != null) {
            action = pushData.optString("action", null);
        }
        if (ACTION_REFRESH.equals(action)) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(ACTION_REFRESH);
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
        }

    }
}
