package com.xenonere.monofluo.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.List;

public class DataRefreshObserver {

    private static DataRefreshObserver sInstance;

    private List<Listener> mListeners;

    public static DataRefreshObserver getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DataRefreshObserver(context);
        }
        return sInstance;
    }

    private DataRefreshObserver(Context context) {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                .getInstance(context.getApplicationContext());
        localBroadcastManager.registerReceiver(mBroadcastReceiver,
                new IntentFilter(PushBroadcastReceiver.ACTION_REFRESH));
        mListeners = new ArrayList<>();
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            for (Listener listener : mListeners) {
                listener.onParseDataChanged();
            }
        }
    };

    public void register(Listener listener) {
        mListeners.add(listener);
    }

    public void unregister(Listener listener) {
        mListeners.remove(listener);
    }

    public interface Listener {

        void onParseDataChanged();
    }

}
