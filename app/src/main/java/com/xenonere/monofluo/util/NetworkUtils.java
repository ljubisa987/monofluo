package com.xenonere.monofluo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;

/**
 * Helper class to be used for logic related to network connectivity
 */
public class NetworkUtils {

    public static boolean hasNetworkConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Verifies if user has network access and shows a message in case he doesn't.
     *
     * @return true if user has network access, false otherwise
     */
    public static boolean verifyNetworkAccess(Context context, FragmentManager fragmentManager) {
        if (!NetworkUtils.hasNetworkConnection(context)) {
            DialogHelper.showNoDataConnectionError(fragmentManager);
            return false;
        }
        return true;
    }

}
