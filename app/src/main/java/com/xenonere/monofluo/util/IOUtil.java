package com.xenonere.monofluo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class IOUtil {

    public static void deleteFolder(File folder) throws IOException {
        delete(folder);
    }

    private static void delete(File file) throws IOException {
        if (file.isDirectory()) {
            for (File c : file.listFiles())
                delete(c);
        }
        if (!file.delete())
            throw new FileNotFoundException("Failed to delete file: " + file);
    }
}
