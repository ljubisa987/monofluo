package com.xenonere.monofluo.util;

public class Contract {

    public static void doesImplements(Object object, Class interfaceClass) {
        if (!interfaceClass.isInstance(object)) {
            throw new IllegalArgumentException(
                    "Object " + object.getClass().getSimpleName() + " must implement "
                            + interfaceClass.getSimpleName());
        }
    }

    public static void isNotNull(Object object) {
        if (object == null) {
            throw new IllegalStateException("Object " + object.getClass().getSimpleName() +
                    " must not be null");
        }
    }

}
