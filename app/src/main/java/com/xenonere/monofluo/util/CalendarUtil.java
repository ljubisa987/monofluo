package com.xenonere.monofluo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarUtil {

    public static SimpleDateFormat DATE_FORMAT_D_M_Y = new SimpleDateFormat("dd/MM/yy",
            Locale.getDefault());

    public static boolean isToday(Calendar calendar) {
        Calendar today = Calendar.getInstance();
        today.setTimeZone(TimeZone.getDefault());
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.compareTo(today) == 0;
    }

}
