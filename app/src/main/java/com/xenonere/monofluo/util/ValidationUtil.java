package com.xenonere.monofluo.util;

import android.text.TextUtils;
import android.util.Patterns;

public class ValidationUtil {

    public static final int MIN_PASSWORD_LENGTH = 6;

    public static boolean isPasswordValid(String password) {
        return !TextUtils.isEmpty(password) && password.length() >= MIN_PASSWORD_LENGTH;
    }

    public static boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
