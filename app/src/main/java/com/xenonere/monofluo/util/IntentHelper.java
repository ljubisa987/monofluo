package com.xenonere.monofluo.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

// TODO: Convert to builder class
public class IntentHelper {

    public static final String EXTRA_REQUEST_CODE = "extraRequestCode";

    /**
     * Sends an intent to launch new activity. Depending on provided args activity will be started
     * in same or in the new task.
     *
     * @param originContext   Context from which new activity will be started, in case of starting
     *                        activity in the same task, MUST be {@link Context} of
     *                        {@link android.app.Activity}.
     * @param activityToStart Class of activity to be started.
     * @param launchInNewTask boolean indicating if {@link android.app.Activity} should be started.
     * @param bundle Bundle, optional param in case that additional extras should be passed to activity.
     */
    public static void launch(Context originContext, Class activityToStart, boolean launchInNewTask,
            @Nullable Bundle bundle) {
        Intent intent = new Intent(originContext, activityToStart);
        if (launchInNewTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        originContext.startActivity(intent);
    }

    /**
     * Helper method to starts activity for result.
     * @param originActivity Activity from which new activity will be started, in case of starting.
     * @param activityToStart Class of activity to be started.
     * @param requestCode Int to be use as an intent request code.
     * @param bundle Bundle, optional param in case that additional extras should be passed to activity.
     */
    public static void launchForResult(Activity originActivity, Class activityToStart,
            int requestCode, @Nullable Bundle bundle) {
        Intent intent = new Intent(originActivity, activityToStart);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        originActivity.startActivityForResult(intent, requestCode);
    }

}
