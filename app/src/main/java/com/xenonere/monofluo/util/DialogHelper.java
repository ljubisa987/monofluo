package com.xenonere.monofluo.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.parse.ParseException;
import com.xenonere.monofluo.R;

public class DialogHelper {

    public static final String TAG_ERROR = "ErrorHandler";

    /**
     * Shows {@link Snackbar} with the provided message.
     *
     * @param view         View in which {@link Snackbar} should be inflated
     * @param messageResId int of string resource to be used as content
     * @param duration     int indicating duration of {@link Snackbar} to be shown, use one of the
     *                     duration from {@link Snackbar} class. In case of
     *                     {@link Snackbar#LENGTH_INDEFINITE} confirmation button will be attached
     *                     to
     */
    public static void showSnackbarkMessage(Context context, View view, @StringRes int messageResId,
            @StringRes int buttonText, int duration,
            @Nullable final View.OnClickListener onButtonClickListener) {
        final Snackbar snackbar = Snackbar.make(view, messageResId, duration);
        if (duration == Snackbar.LENGTH_INDEFINITE) {
            snackbar.setAction(context.getString(buttonText), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                    if (onButtonClickListener != null) {
                        onButtonClickListener.onClick(v);
                    }
                }
            });
        }
        snackbar.show();
    }

    public static void showError(ParseException exception, Context context,
            FragmentManager fragmentManager) {
        showError(exception, context, fragmentManager, null);
    }

    public static void showError(ParseException exception, Context context,
            FragmentManager fragmentManager,
            @Nullable final DialogInterface.OnDismissListener onDismissListener) {
        Log.d(TAG_ERROR, exception.getMessage());
        final String userMessage = parseErrorMessage(exception, context);
        final String positiveAction = context.getString(R.string.action_ok);

        DialogFragment dialogFragment = new DialogFragment() {
            @NonNull
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_dialog_error)
                        .setMessage(userMessage)
                        .setPositiveButton(positiveAction, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (onDismissListener != null) {
                                    onDismissListener.onDismiss(dialog);
                                }
                            }
                        });

                return builder.create();
            }
        };
        dialogFragment.show(fragmentManager, null);
    }

    public static void showError(@StringRes int errorMessage, Context context,
            FragmentManager fragmentManager) {
        final String userMessage = context.getString(errorMessage);
        final String positiveAction = context.getString(R.string.action_ok);

        DialogFragment dialogFragment = new DialogFragment() {
            @NonNull
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_dialog_error)
                        .setMessage(userMessage)
                        .setPositiveButton(positiveAction, null);
                return builder.create();
            }
        };
        dialogFragment.show(fragmentManager, null);
    }

    public static void showNoDataConnectionError(FragmentManager fragmentManager) {
        DialogFragment dialogFragment = new DialogFragment() {
            @NonNull
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_dialog_connection_error)
                        .setMessage(R.string.error_no_connection_message)
                        .setPositiveButton(R.string.action_ok, null);
                return builder.create();
            }
        };
        dialogFragment.show(fragmentManager, null);
    }

    private static String parseErrorMessage(ParseException exception, Context context) {
        switch (exception.getCode()) {
            case ParseException.CONNECTION_FAILED:
                return context.getString(R.string.error_connection_timeout);
            case ParseException.INVALID_EMAIL_ADDRESS:
                return context.getString(R.string.error_invalid_email);
            case ParseException.OBJECT_NOT_FOUND:
                return context.getString(R.string.error_object_not_exist_on_server);
            case ParseException.USERNAME_TAKEN:
                return context.getString(R.string.error_username_taken);
            case ParseException.INVALID_SESSION_TOKEN:
            case ParseException.SESSION_MISSING:
                return context.getString(R.string.error_invalid_session);
            default:
                return context.getString(R.string.error_unknown_error);
        }
    }

}
