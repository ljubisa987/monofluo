package com.xenonere.monofluo.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtil {

    /**
     * Hides the keyboard from the screen. In case that {@link View} for which keyboard is shown is
     * unknown, caller can pass top level view (decor view) from
     * {@link android.app.Activity#getWindow()}.
     *
     * @param view from which token will be used to control the keyboard state
     */
    public static void hideKeyboard(@NonNull View view) {
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Shows the keyboard on the screen.
     *
     * @param view The view which would like to receive soft keyboard input.
     */
    public static void showKeyboard(@NonNull View view) {
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (view != null) {
            inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static boolean isKeyboardShown(@NonNull Context context) {
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);

        return inputManager.isAcceptingText();
    }

}
