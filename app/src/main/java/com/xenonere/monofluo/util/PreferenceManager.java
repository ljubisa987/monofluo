package com.xenonere.monofluo.util;

import com.xenonere.monofluo.MonofluoApp;
import com.xenonere.monofluo.model.Account;

import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

public class PreferenceManager {

    public static final String PREFERENCE_NAME = "monoflou";
    public static final String KEY_DEFAULT_TRANSACTION_ACCOUNT = "defaultTransactionAccount";
    private static final String KEY_ACCOUNT_EXCLUDE = "accountFilter";
    private static final String KEY_DEFAULT_ACCOUNT_CREATED = "defaultAccountCreated";

    private static PreferenceManager sInstance;
    private static SharedPreferences mSharedPreferences;

    private PreferenceManager() {
        mSharedPreferences = MonofluoApp.getInstance().getSharedPreferences(PREFERENCE_NAME, 0);
    }

    public static PreferenceManager getInstance() {
        if (sInstance == null || mSharedPreferences == null) {
            sInstance = new PreferenceManager();
        }
        return sInstance;
    }

    public void setDefaultAccountCreated(){
        mSharedPreferences.edit().putBoolean(KEY_DEFAULT_ACCOUNT_CREATED, true)
                .apply();
    }

    public boolean isDefaultAccountCreated(){
        return mSharedPreferences.getBoolean(KEY_DEFAULT_ACCOUNT_CREATED, false);
    }

    public void saveDefaultTransactionAccount(Account account) {
        mSharedPreferences.edit().putString(KEY_DEFAULT_TRANSACTION_ACCOUNT, account.getObjectId())
                .apply();
    }

    public String getDefaultTransactionAccount() {
        if (!mSharedPreferences.contains(KEY_DEFAULT_TRANSACTION_ACCOUNT)) {
            return null;
        }
        return mSharedPreferences.getString(KEY_DEFAULT_TRANSACTION_ACCOUNT, "");
    }

    public Set<String> getExcludedAccounts() {
        return mSharedPreferences.getStringSet(KEY_ACCOUNT_EXCLUDE, new HashSet<String>());
    }

    public void setExcludedAccounts(Set<String> accountSet) {
        mSharedPreferences.edit().putStringSet(KEY_ACCOUNT_EXCLUDE, accountSet).apply();
    }

    public void clearAccountFilter() {
        mSharedPreferences.edit().remove(KEY_ACCOUNT_EXCLUDE).apply();
    }

    public void clean() {
        mSharedPreferences.edit().clear().apply();
    }
}
