package com.xenonere.monofluo.util;

import android.view.View;

import butterknife.ButterKnife;

public class ViewHelper {

    public static final ButterKnife.Setter<View, Boolean> ENABLED
            = new ButterKnife.Setter<View, Boolean>() {
        @Override
        public void set(View view, Boolean value, int index) {
            view.setEnabled(value);
            view.setClickable(value);
        }
    };

}
