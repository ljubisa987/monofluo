package com.xenonere.monofluo.print;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.model.Transaction;
import com.xenonere.monofluo.util.DialogHelper;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public class PrintTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = PrintTask.class.getSimpleName();

    private final FragmentActivity mActivity;
    private final long mFromDate;
    private final long mToDate;

    /**
     * Starts {@link PrintTask} to print {@link Transaction} list.
     *
     * @param activity Activity from which service will be started.
     * @param fromDate long indicating start date, can be 0, in that case no limit for start date
     *                 will be used in query.
     * @param toDate   long indicating start date, can be 0,  in that case no limit for start date
     *                 will be used in query.
     */
    public PrintTask(FragmentActivity activity, long fromDate, long toDate) {
        mActivity = activity;
        mFromDate = fromDate;
        mToDate = toDate;
    }

    public PrintTask(FragmentActivity activity) {
        this(activity, 0, 0);
    }

    @Override
    protected String doInBackground(Void... params) {
        return prepareTransactionsForPrint();
    }

    @Override
    protected void onPostExecute(String html) {
        printHtml(html);
    }

    private String prepareTransactionsForPrint() {
        ParseQuery<Transaction> query = Transaction.getQuery();
        if (mFromDate != 0) {
            query.whereGreaterThanOrEqualTo(Transaction.KEY_CREATED_AT, mFromDate);
        }
        if (mToDate != 0) {
            query.whereLessThanOrEqualTo(Transaction.KEY_CREATED_AT, mToDate);
        }
        query.addDescendingOrder(Transaction.KEY_TRANSACTION_DATE);
        query.include(Transaction.KEY_ACCOUNT);
        List<Transaction> transactionsList;
        try {
            transactionsList = query.find();
        } catch (ParseException e) {
            Log.e(TAG, "Error while fetching transactions from server.", e);
            if (!mActivity.isFinishing()) {
                DialogHelper.showError(e, mActivity, mActivity.getSupportFragmentManager());
            }
            return null;
        }

        TreeMap<String, List<String>> weeklySortedAmounts = new TreeMap<>();
        for (Transaction transaction : transactionsList) {
            Calendar date = transaction.getTransactionDateCalendar();
            int weekOfYear = date.get(Calendar.WEEK_OF_YEAR);
            int year = date.get(Calendar.YEAR);
            String formattedDate = year + " / " + weekOfYear;

            List<String> amounts;
            if (weeklySortedAmounts.containsKey(formattedDate)) {
                amounts = weeklySortedAmounts.get(formattedDate);
            } else {
                amounts = new ArrayList<>();
                weeklySortedAmounts.put(formattedDate, amounts);
            }
            amounts.add(transaction.getAmount());
        }

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html><body>");
        htmlBuilder.append("<h2>Transactions list</h2><hr>").
                append("<table cellpadding=\"4\"><tr>").
                append("<th>Week</th>").
                append("<th>Sum / week</th>").
                append("<th>Avg / day</th>").
                append("</tr>");

        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        numberFormat.setCurrency(Currency.getInstance(Locale.FRANCE));
        for (String date : weeklySortedAmounts.keySet()) {
            BigDecimal sumPerWeek = new BigDecimal(0);
            List<String> amounts = weeklySortedAmounts.get(date);
            for (String amount : amounts) {
                sumPerWeek = sumPerWeek.add(new BigDecimal(amount));
            }
            BigDecimal averagePerDay = new BigDecimal(sumPerWeek.floatValue() / 7);

            htmlBuilder.append("<tr><th>").append(date).append("</th>").
                    append("<th>").append(numberFormat.format(sumPerWeek)).append("</th><th>").
                    append(numberFormat.format(averagePerDay)).append("</th>");
        }
        htmlBuilder.append("</table></body></html>");

        return htmlBuilder.toString();
    }

    private void printHtml(final String htmlDocument) {
        if (mActivity.isFinishing()) {
            return;
        }

        WebView webView = new WebView(mActivity);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onPageFinished(WebView view, String url) {
                PrintManager printManager = (PrintManager) mActivity.getSystemService(
                        Context.PRINT_SERVICE);
                PrintDocumentAdapter printAdapter = view.createPrintDocumentAdapter();

                String jobName = mActivity.getString(R.string.app_name) + " transactions";
                printManager.print(jobName, printAdapter, new PrintAttributes.Builder()
                        .setMediaSize(PrintAttributes.MediaSize.ISO_A4).build());
            }
        });
        webView.loadDataWithBaseURL(null, htmlDocument, "text/HTML", "UTF-8", null);
    }

}
