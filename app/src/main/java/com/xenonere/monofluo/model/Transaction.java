package com.xenonere.monofluo.model;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.xenonere.monofluo.util.CalendarUtil;

import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

@ParseClassName("Transaction")
public class Transaction extends ParseObject {

    public static final String KEY_ACCOUNT = "account";
    public static final String KEY_OWNER = "owner";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_COMMENT = "comment";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CREATED_AT = "createdAt";
    public static final String KEY_TRANSACTION_DATE = "transactionDate";
    public static final String KEY_OBJECT_ID = "objectId";

    public static final String TYPE_EXPENSE = "expense";

    public static Transaction createNew() {
        Transaction transaction = ParseObject.create(Transaction.class);
        transaction.setTransactionDate(Calendar.getInstance(Locale.getDefault()).getTimeInMillis());
        transaction.setType(TYPE_EXPENSE);
        transaction.setOwner(ParseUser.getCurrentUser());
        return transaction;
    }

    public Account getAccount() {
        return (Account) getParseObject(KEY_ACCOUNT);
    }

    public void setAccount(Account account) {
        put(KEY_ACCOUNT, account);
    }

    public String getAmount() {
        return getString(KEY_AMOUNT);
    }

    public void setAmount(String amount) {
        put(KEY_AMOUNT, new BigDecimal(amount).toString());
    }

    public String getDescription() {
        return getString(KEY_DESCRIPTION);
    }

    public void setTransactionDate(long date) {
        put(KEY_TRANSACTION_DATE, date);
    }

    public long getTransactionDate() {
        return getLong(KEY_TRANSACTION_DATE);
    }

    public Calendar getTransactionDateCalendar() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(getTransactionDate());
        return calendar;
    }

    public String getFormatedDate() {
        return CalendarUtil.DATE_FORMAT_D_M_Y.format(getTransactionDate());
    }

    public void setDescription(String description) {
        put(KEY_DESCRIPTION, description);
    }

    public String getComment() {
        return getString(KEY_COMMENT);
    }

    public void setComment(String comment) {
        put(KEY_COMMENT, comment);
    }

    public String getType() {
        return getString(KEY_TYPE);
    }

    public void setType(String type) {
        put(KEY_TYPE, type);
    }

    public void setOwner(ParseUser user) {
        put(KEY_OWNER, user);
    }

    public ParseUser getOwner() {
        return (ParseUser) getParseObject(KEY_OWNER);
    }

    public static ParseQuery<Transaction> getQuery() {
        return ParseQuery.getQuery(Transaction.class);
    }

    public void invalidateAcl() {
        Account account = getAccount();
        ParseUser currentUser = ParseUser.getCurrentUser();
        // set current user perm
        ParseACL acl = new ParseACL(currentUser);

        // set account owner perm
        if (!account.getOwner().equals(currentUser)) {
            acl.setWriteAccess(account.getOwner(), true);
            acl.setReadAccess(account.getOwner(), true);
        }

        // set read perm to all account members
        if (account.getShareList() != null) {
            for (ParseUser user : account.getShareList()) {
                if (currentUser.getObjectId().equals(user.getObjectId())) {
                    continue;
                }

                acl.setReadAccess(user, true);
                acl.setWriteAccess(user, false);
            }
        }
        setACL(acl);
    }

    public String getFormattedAmount() {
        if (TextUtils.isEmpty(getAmount())) {
            return "";
        }

        return NumberFormat.getCurrencyInstance(Locale.FRANCE).format(new BigDecimal(getAmount()));
    }

    public static String getCurrency() {
        return NumberFormat.getCurrencyInstance(Locale.FRANCE).getCurrency().getSymbol();
    }
}
