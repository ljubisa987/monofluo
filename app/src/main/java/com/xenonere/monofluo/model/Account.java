package com.xenonere.monofluo.model;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.text.TextUtils;

import java.util.Collections;
import java.util.List;

@ParseClassName("Account")
public class Account extends ParseObject {

    public static final String KEY_TITLE = "title";
    public static final String KEY_OWNER = "owner";
    public static final String KEY_SHARE_LIST = "shareList";
    public static final String KEY_IS_SHARED = "isShared";
    public static final String KEY_CREATED_AT = "createdAt";
    public static final String KEY_UPDATED_AT = "updatedAt";

    private boolean mIsSelected;

    public static Account createNew() {
        Account account = ParseObject.create(Account.class);
        account.setShared(false);
        account.setOwner(ParseUser.getCurrentUser());
        account.setIsSelected(true);
        account.setACL(new ParseACL(ParseUser.getCurrentUser()));
        return account;
    }

    public String getTitle() {
        return getString(KEY_TITLE);
    }

    public void setTitle(String title) {
        put(KEY_TITLE, title);
    }

    public void setOwner(ParseUser user) {
        put(KEY_OWNER, user);
    }

    public ParseUser getOwner() {
        return (ParseUser) getParseObject(KEY_OWNER);
    }

    public List<ParseUser> getShareList() {
        return getList(KEY_SHARE_LIST);
    }

    public boolean isShared() {
        return has(KEY_IS_SHARED) && (boolean) get(KEY_IS_SHARED);
    }

    public void setShared(boolean isShared) {
        put(KEY_IS_SHARED, isShared);
    }

    public void addToShareList(ParseUser user) {
        addUnique(KEY_SHARE_LIST, user);
    }

    public void removeFromShareList(ParseUser user) {
        removeAll(KEY_SHARE_LIST, Collections.singletonList(user));
    }

    public static ParseQuery<Account> getQuery() {
        return ParseQuery.getQuery(Account.class);
    }

    public String getCreateDate() {
        return (String) get(KEY_CREATED_AT);
    }

    public String getUpdateDate() {
        return (String) get(KEY_UPDATED_AT);
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        mIsSelected = isSelected;
    }

    public static String generateDefAccountName(String name) {
        if (TextUtils.isEmpty(name)) {
            return "Default account";
        }
        name = name.split(" ")[0];
        return name.substring(0, 1).toUpperCase() + name.substring(1, name.length()) + "'s account";
    }

}