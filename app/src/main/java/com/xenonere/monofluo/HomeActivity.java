package com.xenonere.monofluo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.xenonere.monofluo.account.SharedAccountsListFragment;
import com.xenonere.monofluo.account.UserAccountsListFragment;
import com.xenonere.monofluo.auth.WelcomeActivity;
import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.common.RecyclerViewFragment;
import com.xenonere.monofluo.push.DataRefreshObserver;
import com.xenonere.monofluo.transaction.TransactionsListFragment;
import com.xenonere.monofluo.util.PreferenceManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, DataRefreshObserver.Listener {

    private static final String TAG = HomeActivity.class.toString();

    //@format:off
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;
    //@format:on

    private ActionBarDrawerToggle mDrawerToggle;

    private RecyclerViewFragment mCurrentFragment;

    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        configureDrawer();

        // On initial load, show Transaction fragment, otherwise let system to restore state
        if (savedInstanceState == null) {
            pushFragment(TransactionsListFragment.newInstance(), TransactionsListFragment.TAG);
            selectDefaultNavigationItem();
        } else {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if (!fragments.isEmpty()) {
                mCurrentFragment = (RecyclerViewFragment) fragments.get(fragments.size() - 1);
            }
        }
        mDrawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DataRefreshObserver.getInstance(this).register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DataRefreshObserver.getInstance(this).unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void refreshCurrentFragment() {
        if (mCurrentFragment != null && mCurrentFragment.isVisible()) {
            mCurrentFragment.refreshContent();
        }
    }

    private void configureDrawer() {
        mNavigationView.setNavigationItemSelectedListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        TextView name = ButterKnife.findById(this, R.id.name_textView);
        TextView email = ButterKnife.findById(this, R.id.email_textView);

        name.setText((CharSequence) ParseUser.getCurrentUser().get("name"));
        email.setText(ParseUser.getCurrentUser().getEmail());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        if (menuItem.isChecked()) {
            return true;
        }

        switch (menuItem.getItemId()) {
            case R.id.navigation_logout:
                logout();
                return true;
            case R.id.navigation_transactions:
                pushFragment(TransactionsListFragment.newInstance(), TransactionsListFragment.TAG);
                break;
            case R.id.navigation_userAccounts:
                pushFragment(UserAccountsListFragment.newInstance(), UserAccountsListFragment.TAG);
                break;
            case R.id.navigation_sharedAccounts:
                pushFragment(SharedAccountsListFragment.newInstance(),
                        SharedAccountsListFragment.TAG);
                break;
        }
        mToolbar.setTitle(menuItem.getTitle());
        menuItem.setChecked(true);
        mDrawerLayout.closeDrawers();
        return true;
    }

    private void selectDefaultNavigationItem() {
        MenuItem menuItem = mNavigationView.getMenu().getItem(0);
        menuItem.setChecked(true);
        mToolbar.setTitle(menuItem.getTitle());
    }

    private void pushFragment(RecyclerViewFragment fragment, String tag) {
        mCurrentFragment = fragment;
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragmentContentFrame, fragment, tag).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void logout() {
        PreferenceManager.getInstance().clean();
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                ParseHelper.cleanupParse(MonofluoApp.getInstance());
            }
        });
        launchInNewTask(WelcomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mNavigationView)) {
            mDrawerLayout.closeDrawers();
            return;
        }

        if (isDefaultFragmentShown()) {
            supportFinishAfterTransition();
        } else {
            selectDefaultNavigationItem();
            pushFragment(TransactionsListFragment.newInstance(), TransactionsListFragment.TAG);
        }
    }

    private boolean isDefaultFragmentShown() {
        return TransactionsListFragment.TAG.equals(mCurrentFragment.getTag());
    }

    @Override
    public void onParseDataChanged() {
        refreshCurrentFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            refreshCurrentFragment();
        }
    }
}
