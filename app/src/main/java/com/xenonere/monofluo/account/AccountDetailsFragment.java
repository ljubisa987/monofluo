package com.xenonere.monofluo.account;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.BaseFragment;
import com.xenonere.monofluo.common.DialogEditTextFragment;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.util.DialogHelper;
import com.xenonere.monofluo.util.ViewHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;

public class AccountDetailsFragment extends BaseFragment
        implements GetCallback<ParseObject> {

    public static final int REQUEST_CODE_ADD_PEOPLE = 1;
    public static final int DEFAULT_ALPHA = 255;
    public static final int REDUCED_ALPHA = 100;

    private Account mAccount;
    private String mAccountObjectId;

    @Bind(R.id.enableSharing_switch)
    SwitchCompat mSwitchCompat;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.addPeople_textView)
    TextView mAddPeopleView;
    @Bind(R.id.users_list)
    ListView mListView;
    @Bind(R.id.rename_textView)
    TextView mRenameTextView;
    @Bind(R.id.remove_textView)
    TextView mRemoveTextView;
    @Bind(R.id.rootLayout)
    ViewGroup mRootLayout;
    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;


    @Bind({R.id.enableSharing_switch, R.id.addPeople_textView, R.id.users_list,
            R.id.remove_textView, R.id.rename_textView})
    List<View> mAllFormViews;

    private boolean mIsDataChanged;
    private DialogEditTextFragment mAccountRenamingFragment;

    @Override
    protected int getContentView() {
        return R.layout.fragment_account_details;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupActivityAppBar(R.drawable.ic_close_white_24dp);
        mListView.setEmptyView(ButterKnife.findById(view, android.R.id.empty));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsDataChanged) {
            markDataChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mAccountObjectId", mAccountObjectId);
        outState.putBoolean("mIsDataChanged", mIsDataChanged);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mAccountObjectId = savedInstanceState.getString("mAccountObjectId");
            mIsDataChanged = savedInstanceState.getBoolean("mIsDataChanged");
            mAccountRenamingFragment = (DialogEditTextFragment)
                    getChildFragmentManager().findFragmentByTag(DialogEditTextFragment.TAG);
            if (mAccountRenamingFragment != null) {
                mAccountRenamingFragment.setListener(mAccountEditNameCallback);
            }
            if (TextUtils.isEmpty(mAccountObjectId)) {
                prepareNewAccount();
            } else {
                load(mAccountObjectId);
            }
        }
    }

    public void load(String objectId) {
        ParseQuery<Account> query = Account.getQuery().whereEqualTo("objectId", objectId);
        query.include(Account.KEY_SHARE_LIST);
        query.include(Account.KEY_OWNER);
        query.getFirstInBackground(new GetCallback<Account>() {
            @Override
            public void done(Account account, ParseException e) {
                if (e == null) {
                    if (!isDetached() && getActivity() != null) {
                        mAccount = account;
                        populateUi();
                    }
                } else {
                    if (isVisible()) {
                        DialogHelper.showError(e, getActivity(), getFragmentManager(),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        getActivity().finish();
                                    }
                                });
                    }
                }

            }
        });
    }

    @OnItemLongClick(R.id.users_list)
    boolean removePeople(int position) {
        markDataChanged();
        final ParseUser user = mAccount.getShareList().get(position);
        Toast.makeText(getActivity(), "Removed from share list: " + user.getEmail(),
                Toast.LENGTH_SHORT).show();
        ParseACL mAccountACL = mAccount.getACL();
        mAccountACL.setWriteAccess(user, false);
        mAccountACL.setReadAccess(user, false);
        mAccount.setACL(mAccountACL);
        mAccount.removeFromShareList(user);
        mAccount.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParseHelper.notifyAccountUserRemoved(mAccount, user);

                if (!isVisible()) return;
                if (e != null) {
                    DialogHelper.showError(e, getActivity(), getFragmentManager(),
                            new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    getActivity().finish();
                                }
                            });
                } else {
                    invalidateWidgetsState();
                }
            }
        });
        populateAccountShareList();
        return true;
    }

    private void addPeople(final String userId) {
        markDataChanged();
        ParseUser.getQuery().getInBackground(userId, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (e == null) {
                    mAccount.addToShareList(object);
                    final ParseACL mAccountACL = mAccount.getACL();
                    mAccountACL.setReadAccess(userId, true);
                    mAccountACL.setWriteAccess(userId, false);
                    mAccount.setACL(mAccountACL);
                    mAccount.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            ParseHelper.notifyAccountUpdate(mAccount);
                             if (getActivity() == null) return;
                            if (e == null && !isDetached()) {
                                Toast.makeText(getActivity(), "New share user added!",
                                        Toast.LENGTH_SHORT).show();
                                populateAccountShareList();
                            } else if (isVisible()) {
                                DialogHelper.showError(e, getActivity(), getFragmentManager());
                            }

                        }
                    });

                } else {
                    DialogHelper.showError(e, getActivity(), getFragmentManager());
                }
            }
        });
    }

    public void populateUi() {
        mToolbar.setTitle(mAccount.getTitle());
        if (!ParseHelper.isCurrentUser(mAccount.getOwner())) {
            mToolbar.setSubtitle(mAccount.getOwner().getUsername());
        }
        mSwitchCompat.setOnCheckedChangeListener(null);
        mSwitchCompat.setChecked(mAccount.isShared());
        mSwitchCompat.setOnCheckedChangeListener(mOnCheckedChangeListener);
        populateAccountShareList();
        invalidateWidgetsState();
    }

    private void populateAccountShareList() {
        UsersListAdapter simpleArrayAdapter = new UsersListAdapter(getActivity());
        if (mAccount.getShareList() != null) {
            simpleArrayAdapter.addAll(mAccount.getShareList());
            mListView.setAdapter(simpleArrayAdapter);
        }
    }

    private void invalidateWidgetsState() {
        mRootLayout.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);

        boolean isEditable = false;
        boolean isAccountShared = false;
        if (mAccount != null && mAccount.getACL().getWriteAccess(ParseUser.getCurrentUser())) {
            isEditable = true;
            isAccountShared = mAccount.isShared();
        }
        ButterKnife.apply(mAllFormViews, ViewHelper.ENABLED, isEditable);

        setAddPeopleButtonState(isEditable && isAccountShared);

        mRemoveTextView.getCompoundDrawables()[1].setAlpha(getAlpha(isEditable));
        mRenameTextView.getCompoundDrawables()[1].setAlpha(getAlpha(isEditable));
    }

    private void setAddPeopleButtonState(boolean isAddPeopleAllowed) {
        mAddPeopleView.setClickable(isAddPeopleAllowed);
        mAddPeopleView.setEnabled(isAddPeopleAllowed);
        mAddPeopleView.getCompoundDrawables()[1].setAlpha(getAlpha(isAddPeopleAllowed));
    }

    private int getAlpha(boolean isEnabled) {
        return isEnabled ? DEFAULT_ALPHA : REDUCED_ALPHA;
    }

    @Override
    public void done(ParseObject parseObject, ParseException e) {

    }

    public void loadAccount(String objectId) {
        mAccountObjectId = objectId;
        load(objectId);
    }

    public void newAccount() {
        prepareNewAccount();
        mAccountRenamingFragment = DialogEditTextFragment.newInstance(R.string.title_account_name, mAccount.getTitle());
        mAccountRenamingFragment.show(getChildFragmentManager(), DialogEditTextFragment.TAG);
        mAccountRenamingFragment.setListener(mAccountEditNameCallback);
    }

    private void prepareNewAccount() {
        mProgressBar.setVisibility(View.GONE);
        mToolbar.setTitle(R.string.new_account_title);
        mAccount = Account.createNew();
    }

    private final DialogEditTextFragment.Listener mAccountEditNameCallback
            = new DialogEditTextFragment.Listener() {
        @Override
        public void onDialogFragmentInputCompleted(String value) {
            mProgressBar.setVisibility(View.VISIBLE);
            mAccount.setTitle(value);
            mToolbar.setTitle(value);
            mSwitchCompat.setOnCheckedChangeListener(mOnCheckedChangeListener);
            mAccount.saveInBackground(mSaveCallback);
        }

        @Override
        public void onDialogFragmentCanceled() {
            if (TextUtils.isEmpty(mAccount.getTitle())) {
                getActivity().finish();
            }
        }
    };

    @OnClick(R.id.rename_textView)
    void renameAccount() {
        DialogEditTextFragment fragment = DialogEditTextFragment.newInstance(
                R.string.title_rename_account, mAccount.getTitle());
        fragment.setListener(mAccountEditNameCallback);
        fragment.show(getChildFragmentManager(), DialogEditTextFragment.TAG);
    }

    @OnClick(R.id.addPeople_textView)
    void onAddPeopleClick() {
        startActivityForResult(new Intent(getActivity(), AddPeopleActivity.class),
                REQUEST_CODE_ADD_PEOPLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_PEOPLE && resultCode == Activity.RESULT_OK) {
            final String userId = data.getStringExtra(AddPeopleActivity.EXTRA_OBJECT_ID);
            addPeople(userId);
        }
    }

    @OnClick(R.id.remove_textView)
    void removeAccount() {
        markDataChanged();
        if (mAccount != null) {
            mAccount.deleteInBackground(new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    ParseHelper.notifyAccountUpdate(mAccount);
                    if (isVisible()) {
                        if (e == null) {
                            markDataChanged();
                            getActivity().finish();
                        } else {
                            DialogHelper.showError(e, getActivity(), getFragmentManager(),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            getActivity().finish();
                                        }
                                    });
                        }
                    }
                }
            });
        }
    }

    private final CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            markDataChanged();
            setAddPeopleButtonState(isChecked);
            if (mAccount != null) {
                mAccount.setShared(isChecked);
                mAccount.saveInBackground(mSaveCallback);
            }
        }
    };

    final SaveCallback mSaveCallback = new SaveCallback() {
        @Override
        public void done(ParseException e) {
            markDataChanged();
            ParseHelper.notifyAccountUpdate(mAccount);
            if (!isVisible()) return;
            if (e != null) {
                DialogHelper.showError(e, getActivity(), getFragmentManager(),
                        new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                getActivity().finish();
                            }
                        });
            } else {
                invalidateWidgetsState();
            }
        }
    };

    private void markDataChanged() {
        getActivity().setResult(Activity.RESULT_OK);
        mIsDataChanged = true;
    }

}
