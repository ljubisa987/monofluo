package com.xenonere.monofluo.account;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.model.Account;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AccountsRecyclerAdapter
        extends RecyclerView.Adapter<AccountsRecyclerAdapter.ViewHolder> {

    private final Context mContext;
    private List<Account> mAccountList;
    private View.OnClickListener mOnClickListener;
    private ParseUser mCurrentUser;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.accountTitle_textView)
        public TextView mAccountTitle;
        @Bind(R.id.accountShare_imageView)
        public ImageView mAccountShareIcon;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public AccountsRecyclerAdapter(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        mOnClickListener = onClickListener;
        mAccountList = new ArrayList<>();
        mCurrentUser = ParseUser.getCurrentUser();
    }

    public List<Account> getAccountList() {
        return mAccountList;
    }

    public void setAccountsList(List<Account> accounts) {
        mAccountList = accounts;
        notifyDataSetChanged();
    }

    @Override
    public AccountsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_list_item, parent, false);
        v.setOnClickListener(mOnClickListener);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Account account = mAccountList.get(position);
        holder.mAccountTitle.setText(account.getTitle());
        Drawable accountShareIcon;
        if (account.isShared()) {
            accountShareIcon = ContextCompat.getDrawable(mContext, R.drawable.ic_group_black_24dp);
        } else {
            accountShareIcon = ContextCompat.getDrawable(mContext, R.drawable.ic_person_black_24dp);
        }

        boolean isUserAccount = mCurrentUser.getObjectId().equals(account.getOwner().getObjectId());
        Drawable accountIndicator = ContextCompat.getDrawable(mContext, isUserAccount ? R.drawable.ic_folder_black_24dp :
                R.drawable.ic_folder_shared_black_24dp);
        accountIndicator.setBounds(0, 0, accountIndicator.getIntrinsicWidth(), accountIndicator.getIntrinsicHeight());
        int tintColor = mContext.getResources().getColor(R.color.textColorSecondary);
        DrawableCompat.setTint(accountShareIcon, tintColor);
        DrawableCompat.setTint(accountIndicator, tintColor);

        holder.mAccountTitle.setCompoundDrawables(accountIndicator, null, null, null);
        holder.mAccountShareIcon.setImageDrawable(accountShareIcon);
    }

    @Override
    public int getItemCount() {
        return mAccountList.size();
    }
}