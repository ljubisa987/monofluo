package com.xenonere.monofluo.account;

import com.parse.ParseUser;
import com.xenonere.monofluo.R;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class UsersListAdapter extends ArrayAdapter<ParseUser> {

    private final LayoutInflater mInflater;
    private final Context mContext;

    public UsersListAdapter(Context context) {
        super(context, -1);
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView;
        final ImageView imageView;
        if (convertView != null) {
            textView = (TextView) convertView.findViewById(R.id.textView);
            imageView = (ImageView) convertView.findViewById(R.id.imageView);
        } else {
            convertView = mInflater.inflate(R.layout.simple_list_item, parent, false);
            textView = (TextView) convertView.findViewById(R.id.textView);
            imageView = (ImageView) convertView.findViewById(R.id.imageView);
        }

        imageView.setImageDrawable(
                ContextCompat.getDrawable(mContext, R.drawable.ic_person_black_24dp));
        textView.setText(getItem(position).getEmail());
        return convertView;
    }
}
