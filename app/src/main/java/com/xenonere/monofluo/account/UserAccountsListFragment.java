package com.xenonere.monofluo.account;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.RecyclerViewFragment;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.util.DialogHelper;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.List;

public class UserAccountsListFragment extends RecyclerViewFragment<AccountsRecyclerAdapter>
        implements View.OnClickListener {

    public static final String TAG = UserAccountsListFragment.class.toString();

    public static UserAccountsListFragment newInstance() {
        return new UserAccountsListFragment();
    }

    @Override
    protected AccountsRecyclerAdapter onAdapterCreate() {
        return new AccountsRecyclerAdapter(getActivity(), this);
    }

    @Override
    protected boolean hasAddButton() {
        return true;
    }

    @Override
    protected int getNoContentText() {
        return R.string.info_no_accounts;
    }

    @Override
    public void refreshContent() {
        loadAccounts();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadAccounts();
    }

    private void loadAccounts() {
        updateLoadingView(true);
        ParseQuery<Account> query = ParseQuery.getQuery(Account.class);
        query.whereEqualTo(Account.KEY_OWNER, ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<Account>() {
            @Override
            public void done(List<Account> accounts, ParseException e) {
                updateLoadingView(false);
                if (e == null) {
                    getAdapter().setAccountsList(accounts);
                    showNoContent(accounts.isEmpty());
                } else {
                    if (isVisible()) {
                        DialogHelper.showError(e, getActivity(),
                                getActivity().getSupportFragmentManager());
                    }
                }
            }
        });
    }

    @Override
    protected void onAddButtonClick() {
        AccountDetailsActivity.createAccount(getActivity());
    }

    @Override
    public void onClick(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);
        if (position >= 0 && position <= getAdapter().getAccountList().size()) {
            Account account = getAdapter().getAccountList()
                    .get(position);
            AccountDetailsActivity.showAccountDetails(getActivity(), account.getObjectId());
        }
    }
}
