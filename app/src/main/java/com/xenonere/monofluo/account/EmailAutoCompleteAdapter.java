package com.xenonere.monofluo.account;

import android.content.Context;
import android.util.Log;
import android.widget.Filter;
import android.widget.Filterable;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class EmailAutoCompleteAdapter extends UsersListAdapter implements Filterable {

    private List<ParseUser> mUsersList;

    public EmailAutoCompleteAdapter(Context context) {
        super(context);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                if (constraint != null) {
                    ParseQuery query = ParseUser.getQuery()
                            .whereContains("email", (String) constraint);
                    query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
                    query.setLimit(10);
                    try {
                        //noinspection unchecked
                        mUsersList = query.find();
                    } catch (ParseException e) {
                        Log.e("EmailAutoCAdapter", "Error while fetching user data.", e);
                    }

                }
                if (mUsersList == null) {
                    mUsersList = new ArrayList<>();
                }

                final FilterResults filterResults = new FilterResults();
                filterResults.values = mUsersList;
                filterResults.count = mUsersList.size();
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(final CharSequence contraint,
                    final FilterResults results) {
                clear();
                addAll(mUsersList);

                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(final Object resultValue) {
                return resultValue == null ? "" : ((ParseUser) resultValue).getEmail();
            }
        };
    }
}