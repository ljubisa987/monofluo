package com.xenonere.monofluo.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.util.Contract;

public class AccountDetailsActivity extends BaseActivity {

    public static final String EXTRA_OBJECT_ID = "extraObjectId";

    public static void createAccount(Activity activity) {
        Intent intent = new Intent(activity, AccountDetailsActivity.class);
        intent.setAction(Intent.ACTION_INSERT);
        activity.startActivityForResult(intent, 20);
    }

    public static void showAccountDetails(Activity activity, String objectId) {
        Intent intent = new Intent(activity, AccountDetailsActivity.class);
        intent.putExtra(EXTRA_OBJECT_ID, objectId);
        intent.setAction(Intent.ACTION_VIEW);
        activity.startActivityForResult(intent, 21);
    }


    @Override
    protected int getContentView() {
        return R.layout.activity_account_details;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState == null) {
            AccountDetailsFragment fragment = (AccountDetailsFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.accountDetailsFragment);
            if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
                String objectId = getIntent().getStringExtra(EXTRA_OBJECT_ID);
                Contract.isNotNull(objectId);
                fragment.loadAccount(objectId);
            } else {
                fragment.newAccount();
            }
        }
    }
}
