package com.xenonere.monofluo.account;

import com.xenonere.monofluo.common.BaseFragment;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.util.Contract;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import butterknife.Bind;

public class AddPeopleFragment extends BaseFragment {

    @Bind(R.id.email_autoComplete)
    AutoCompleteTextView mAutoCompleteTextView;

    private EmailAutoCompleteAdapter mEmailCompleteAdapter;
    private Listener mListener;
    private String mSelectedEmail;
    private String mSelectedUserId;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Contract.doesImplements(activity, Listener.class);

        mListener = (Listener) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mSelectedEmail", mSelectedEmail);
        outState.putString("mSelectedUserId", mSelectedUserId);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mSelectedEmail = savedInstanceState.getString("mSelectedEmail");
            mSelectedUserId = savedInstanceState.getString("mSelectedUserId");
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_add_people;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEmailCompleteAdapter = new EmailAutoCompleteAdapter(getActivity());
        mAutoCompleteTextView.setAdapter(mEmailCompleteAdapter);

        mAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedEmail = mEmailCompleteAdapter.getItem(position).getEmail();
                mSelectedUserId = mEmailCompleteAdapter.getItem(position).getObjectId();
            }
        });
        setupActivityAppBar(R.drawable.ic_close_white_24dp);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_people_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_save == item.getItemId()) {
            if (TextUtils.isEmpty(mAutoCompleteTextView.getText())) {
                Toast.makeText(getActivity(), "Please choose user email first.", Toast.LENGTH_SHORT)
                        .show();

            } else {
                if (mSelectedEmail != null && mAutoCompleteTextView.getText().toString()
                        .equals(mSelectedEmail)) {
                    mListener.onUserSelected(mSelectedUserId);
                } else {
                    mSelectedEmail = null;
                    mSelectedUserId = null;
                    Toast.makeText(getActivity(), R.string.error_select_user_from_list,
                            Toast.LENGTH_LONG).show();
                }
            }
            return true;
        }
        return false;
    }

    public interface Listener {

        void onUserSelected(String userObjectId);
    }
}
