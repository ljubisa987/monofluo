package com.xenonere.monofluo.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.util.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AccountFilterDialog extends DialogFragment {

    public static final String TAG = AccountFilterDialog.class.getSimpleName();

    public static AccountFilterDialog newInstance() {
        return new AccountFilterDialog();
    }

    private Listener mListener;
    private ListView mAccountListView;
    private List<Account> mAccountsList;
    private View mEmptyView;
    private Button mSaveButton;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.title_account_filter);
        View view = alertDialog.getLayoutInflater().inflate(R.layout.account_filter, null);
        mAccountListView = (ListView) view.findViewById(R.id.accounts_listView);
        mEmptyView = view.findViewById(R.id.empty);
        alertDialog.setView(view);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.action_save),
                mOnSaveClickListener);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.action_cancel),
                (DialogInterface.OnClickListener) null);
        return alertDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mSaveButton = ((AlertDialog)getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        mSaveButton.setEnabled(false);
        fetchAccounts();
    }

    private void fetchAccounts() {
        ParseQuery<Account> userAccount = Account.getQuery();
        userAccount.whereEqualTo(Account.KEY_OWNER, ParseUser.getCurrentUser());
        ParseQuery<Account> sharedAccount = Account.getQuery();
        sharedAccount.whereEqualTo(Account.KEY_SHARE_LIST, ParseUser.getCurrentUser());
        List<ParseQuery<Account>> queries = new ArrayList<>();

        queries.add(userAccount);
        queries.add(sharedAccount);
        ParseQuery<Account> mainQuery = ParseQuery.or(queries);
        mainQuery.findInBackground(new FindCallback<Account>() {
            @Override
            public void done(List<Account> accountList, ParseException e) {
                mAccountsList = accountList;
                prepareAccountFilter();
                populateAccountList();
            }
        });
    }

    void prepareAccountFilter() {
        Set<String> excludedAccounts = PreferenceManager.getInstance().getExcludedAccounts();
        for (int i = 0; i < mAccountsList.size(); i++) {
            Account account = mAccountsList.get(i);
            account.setIsSelected(!excludedAccounts.contains(account.getObjectId()));
        }
    }

    private void populateAccountList() {
        AccountListAdapter accountListAdapter = new AccountListAdapter(getActivity(), true);
        accountListAdapter.addAll(mAccountsList);
        mAccountListView.setAdapter(accountListAdapter);
        if (mAccountsList.isEmpty()) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mSaveButton.setEnabled(true);
        }
    }

    private DialogInterface.OnClickListener mOnSaveClickListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            HashSet<String> accountSet = new HashSet<>();
            for (int i = 0; i < mAccountsList.size(); i++) {
                if (!mAccountsList.get(i).isSelected()) {
                    accountSet.add(mAccountsList.get(i).getObjectId());
                }
            }
            PreferenceManager.getInstance().setExcludedAccounts(accountSet);
            if (mListener != null) {
                mListener.onFilterChanged();
            }
        }
    };

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {

        void onFilterChanged();
    }
}
