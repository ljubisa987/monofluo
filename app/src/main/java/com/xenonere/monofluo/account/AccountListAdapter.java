package com.xenonere.monofluo.account;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.model.Account;

public class AccountListAdapter extends ArrayAdapter<Account> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final boolean mCheckable;

    public AccountListAdapter(Context context, boolean checkable) {
        super(context, -1);
        mContext = context;
        mCheckable = checkable;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Account account = getItem(position);
        final TextView textView;
        final ImageView imageView;
        final CheckBox checkBox;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.simple_list_item, parent, false);
        }
        textView = (TextView) convertView.findViewById(R.id.textView);
        imageView = (ImageView) convertView.findViewById(R.id.imageView);
        checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

        textView.setText(account.getTitle());

        Drawable accountIcon;
        if (ParseHelper.isCurrentUser(account.getOwner())) {
            accountIcon = ContextCompat.getDrawable(mContext, R.drawable.ic_folder_black_24dp);
        } else {
            accountIcon = ContextCompat.getDrawable(mContext, R.drawable.ic_folder_shared_black_24dp);
        }
        imageView.setImageDrawable(accountIcon);
        int tintColor = mContext.getResources().getColor(R.color.textColorSecondary);
        DrawableCompat.setTint(accountIcon, tintColor);

        if (mCheckable) {
            checkBox.setVisibility(View.VISIBLE);
            checkBox.setChecked(account.isSelected());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.performClick();
                    boolean isChecked = checkBox.isChecked();
                    account.setIsSelected(isChecked);
                }
            });
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
