package com.xenonere.monofluo.account;

import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.R;

import android.app.Activity;
import android.content.Intent;

public class AddPeopleActivity extends BaseActivity implements AddPeopleFragment.Listener {

    public static final String EXTRA_OBJECT_ID = "extraObjectId";

    @Override
    protected int getContentView() {
        return R.layout.activity_add_people;
    }

    @Override
    public void onUserSelected(String userObjectId) {
        Intent result = new Intent();
        result.putExtra(EXTRA_OBJECT_ID, userObjectId);
        setResult(Activity.RESULT_OK, result);
        finish();
    }

}
