package com.xenonere.monofluo;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.model.Transaction;

import android.app.Application;
import android.graphics.Typeface;

public class MonofluoApp extends Application {

    private static MonofluoApp sInstance;
    private static Typeface sLogoTypeface;
    private static Typeface sMotoTypeface;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sLogoTypeface = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Medium.ttf");
        sMotoTypeface = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-LightItalic.ttf");

        ParseObject.registerSubclass(Transaction.class);
        ParseObject.registerSubclass(Account.class);
        ParseACL defaultACL = new ParseACL();
        ParseACL.setDefaultACL(defaultACL, true);
        Parse.initialize(this, getString(R.string.parse_app_id),
                getString(R.string.parse_client_id));
    }

    public static MonofluoApp getInstance() {
        return sInstance;
    }

    public static Typeface getLogoFont() {
        return sLogoTypeface;
    }

    public static Typeface getMotoFont() {
        return sMotoTypeface;
    }

}
