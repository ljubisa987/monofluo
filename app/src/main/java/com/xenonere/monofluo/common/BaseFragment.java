package com.xenonere.monofluo.common;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xenonere.monofluo.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    @Bind(R.id.toolbar)
    @Nullable
    protected Toolbar mToolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(getContentView(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    protected abstract int getContentView();

    protected void setupActivityAppBar(@DrawableRes int navigationIcon){
        BaseActivity activity =((BaseActivity) getActivity());
        activity.setSupportActionBar(mToolbar);
        activity.simpleAppBarConfiguration(navigationIcon);
    }

}
