package com.xenonere.monofluo.common;

import com.xenonere.monofluo.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

public class DialogEditTextFragment extends DialogFragment {

    public static String TAG = DialogEditTextFragment.class.getSimpleName();

    private static final int NO_VALUE = 0;
    private static final String EXTRA_TITLE = "extraTitle";
    private static final String EXTRA_TITLE_ICON = "extraTitleIcon";
    private static final String EXTRA_CONTENT_POSITIVE_BUTTON = "extraPositiveContent";
    private static final String EXTRA_CONTENT_NEGATIVE_BUTTON = "extraNegativeContent";
    private static final String EXTRA_PREDEFINED_TEXT = "extraPredefinedText";
    private static final String EXTRA_CONTENT_MESSAGE = "extraContentMessage";
    private AlertDialog mAlertDialog;

    public static DialogEditTextFragment newInstance(@StringRes int title,
            @Nullable String predefinedText) {
        return newInstance(title, predefinedText, NO_VALUE, NO_VALUE, NO_VALUE, NO_VALUE);
    }

    public static DialogEditTextFragment newInstance(@StringRes int title,
            @Nullable String predefinedText, @StringRes int contentMessage) {
        return newInstance(title, predefinedText, contentMessage, NO_VALUE, NO_VALUE, NO_VALUE);
    }

    public static DialogEditTextFragment newInstance(@StringRes int title,
            @Nullable String predefinedText, @StringRes int contentMessage,
            @DrawableRes int titleIcon, int contentPositiveButton, int contentNegativeButton) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_TITLE, title);
        args.putInt(EXTRA_CONTENT_MESSAGE, contentMessage);
        args.putInt(EXTRA_TITLE_ICON, titleIcon);
        args.putInt(EXTRA_CONTENT_POSITIVE_BUTTON, contentPositiveButton);
        args.putInt(EXTRA_CONTENT_NEGATIVE_BUTTON, contentNegativeButton);
        if (predefinedText == null) {
            predefinedText = "";
        }
        args.putString(EXTRA_PREDEFINED_TEXT, predefinedText);

        DialogEditTextFragment fragment = new DialogEditTextFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Bind(R.id.editText)
    EditText mEditText;
    private Listener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAlertDialog = new AlertDialog.Builder(
                getActivity()).create();
        int title = getArguments().getInt(EXTRA_TITLE);
        int contentMessage = getArguments().getInt(EXTRA_CONTENT_MESSAGE);
        int titleIcon = getArguments().getInt(EXTRA_TITLE_ICON);
        int contentPositiveButton = getArguments().getInt(EXTRA_CONTENT_POSITIVE_BUTTON);
        int contentNegativeButton = getArguments().getInt(EXTRA_CONTENT_NEGATIVE_BUTTON);

        View view = mAlertDialog.getLayoutInflater()
                .inflate(R.layout.fragment_dialog_editext, null);
        mAlertDialog.setView(view);

        if (title != 0) {
            mAlertDialog.setTitle(title);
        }

        if (contentMessage != 0) {
            mAlertDialog.setMessage(getString(contentMessage));
        }

        if (titleIcon != 0) {
            mAlertDialog.setIcon(titleIcon);
        }

        if (contentPositiveButton == 0) {
            contentPositiveButton = R.string.action_ok;
        }

        if (contentNegativeButton == 0) {
            contentNegativeButton = R.string.action_cancel;
        }
        mAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(contentPositiveButton),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) {
                            mListener
                                    .onDialogFragmentInputCompleted(mEditText.getText().toString());
                        }
                    }
                });
        mAlertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(contentNegativeButton),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) {
                            mListener.onDialogFragmentCanceled();
                        }
                    }
                });
        mAlertDialog.setCanceledOnTouchOutside(false);
        ButterKnife.bind(this, view);

        return mAlertDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        setPredefinedTextIfNeeded();
        updatePositiveButton(!TextUtils.isEmpty(mEditText.getText()) && !isSameAsPredefinedText(
                mEditText.getText().toString()));
    }

    private boolean isSameAsPredefinedText(CharSequence text) {
        return getArguments().getString(EXTRA_PREDEFINED_TEXT).equals(text);
    }

    private void updatePositiveButton(boolean isEnabled) {
        mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(isEnabled);
        mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setClickable(isEnabled);
    }

    private void setPredefinedTextIfNeeded() {
        String predefinedText = getArguments().getString(EXTRA_PREDEFINED_TEXT);
        if (!TextUtils.isEmpty(predefinedText)) {
            mEditText.setText(predefinedText);
            mEditText.setSelection(0, predefinedText.length());
        }
    }

    @OnTextChanged(R.id.editText)
    void onTextChanged(CharSequence text) {
        updatePositiveButton(text.length() > 0 && !isSameAsPredefinedText(text));
    }

    @OnEditorAction(R.id.editText)
    boolean onEditorAction(TextView t, int actionId, KeyEvent key) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            dismiss();
            mListener.onDialogFragmentInputCompleted(mEditText.getText().toString());
        }
        return true;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mListener.onDialogFragmentCanceled();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {

        void onDialogFragmentInputCompleted(String value);

        void onDialogFragmentCanceled();

    }

}
