package com.xenonere.monofluo.common;

import android.content.Context;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.xenonere.monofluo.model.Account;
import com.xenonere.monofluo.util.IOUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseHelper {

    public static final String TAG = "ParseHelper";

    public static void notifyAccountUserRemoved(Account account, ParseUser removedUser) {
        notifyAccountUpdate(account, removedUser);
    }

    public static void notifyAccountUpdate(Account account) {
        notifyAccountUpdate(account, null);
    }

    private static void notifyAccountUpdate(Account account, ParseUser removedUser) {
        List<String> userIds = new ArrayList<>();
        List<ParseUser> shareList = account.getShareList();
        if (shareList != null) {
            for (ParseUser user : shareList) {
                userIds.add(user.getObjectId());
            }
        }
        userIds.add(account.getOwner().getObjectId());
        userIds.remove(ParseUser.getCurrentUser().getObjectId());
        if (removedUser != null) {
            userIds.add(removedUser.getObjectId());
        }
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereContainedIn("user", userIds);
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        try {
            push.setData(new JSONObject(
                    "{'action':'actionRefresh'}"));
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
        push.sendInBackground();
    }

    public static boolean isCurrentUser(ParseUser user) {
        return ParseUser.getCurrentUser().getObjectId().equals(user.getObjectId());
    }

    public static void addUserToInstallation() {
        // Add user to installation in order to filter him for push
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("user", ParseUser.getCurrentUser().getObjectId());
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.e(TAG, "Error while saving installation.", e);

                }
            }
        });
    }

    public static void cleanupParse(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    IOUtil.deleteFolder(context.getDir("Parse", Context.MODE_PRIVATE));
                    IOUtil.deleteFolder(context.getCacheDir());
                } catch (IOException e) {
                    Log.e(TAG, "Error removing content.", e);
                }
            }
        }).start();
    }


}
