package com.xenonere.monofluo.common;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public static String TAG = DatePickerFragment.class.getSimpleName();

    private static final String EXTRA_TITLE = "extraTitle";

    public static DatePickerFragment newInstance(@StringRes int title) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_TITLE, title);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Listener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getDefault());
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog alertDialog = new DatePickerDialog(getActivity(), this, year, month, day);

        int title = getArguments().getInt(EXTRA_TITLE);

        if (title != 0) {
            alertDialog.setTitle(title);
        }
        alertDialog.setCanceledOnTouchOutside(false);

        return alertDialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mListener.onDatePickerCanceled();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = GregorianCalendar.getInstance(Locale.getDefault());
        calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        mListener.onDatePickerDone(calendar);
    }

    public interface Listener {

        void onDatePickerDone(Calendar selectedDate);

        void onDatePickerCanceled();

    }

}
