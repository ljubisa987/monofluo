package com.xenonere.monofluo.common;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.util.IntentHelper;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ButterKnife.bind(this);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

    }

    @Nullable
    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected abstract int getContentView();

    protected void launchInSameTask(Class activityToStart) {
        IntentHelper.launch(this, activityToStart, false, null);
    }

    protected void launchInNewTask(Class activityToStart) {
        IntentHelper.launch(this, activityToStart, true, null);
    }

    public void simpleAppBarConfiguration(@DrawableRes int navigationIcon) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(navigationIcon);
        }
    }

}
