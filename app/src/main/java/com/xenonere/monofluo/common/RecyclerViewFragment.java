package com.xenonere.monofluo.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xenonere.monofluo.R;

import butterknife.Bind;
import butterknife.OnClick;

public abstract class RecyclerViewFragment<T extends RecyclerView.Adapter> extends BaseFragment {

    @Bind(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @Bind(R.id.add_button)
    protected FloatingActionButton mAddButton;
    @Bind(R.id.empty)
    protected TextView mEmptyTextView;
    @Bind(R.id.progressBar)
    protected ProgressBar mProgressBar;

    protected LinearLayoutManager mLayoutManager;

    private T mAdapter;

    @Override
    protected int getContentView() {
        return R.layout.fragment_recycler_view;
    }

    protected abstract T onAdapterCreate();

    protected abstract boolean hasAddButton();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!hasAddButton()) {
            mAddButton.setVisibility(View.GONE);
        }
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = onAdapterCreate();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new ItemSeparatorDecoration(5));
        mEmptyTextView.setText(getNoContentText());
    }

    public abstract void refreshContent();

    protected int getNoContentText() {
        return R.string.info_no_content;
    }

    protected void showNoContent(boolean show){
        if (isVisible()) {
            mEmptyTextView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected void updateLoadingView(boolean isLoading) {
        if (isVisible()) {
            mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.add_button)
    protected void onAddButtonClick() {
        //nop
    }

    protected T getAdapter() {
        return mAdapter;
    }

}
