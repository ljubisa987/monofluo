package com.xenonere.monofluo.auth;

import com.xenonere.monofluo.R;
import com.xenonere.monofluo.util.Contract;
import com.xenonere.monofluo.util.ValidationUtil;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class ForgotPasswordFragment extends DialogFragment {

    public static String TAG = ForgotPasswordFragment.class.getSimpleName();

    @Bind(R.id.email_textInput)
    TextInputLayout mEmailWrapper;
    private Listener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Contract.doesImplements(activity, Listener.class);
        mListener = (Listener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container);
        ButterKnife.bind(this, view);

        getDialog().setTitle(R.string.action_forgot);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return view;
    }

    @OnEditorAction(R.id.email_editText)
    boolean onEditorAction(TextView t, int actionId, KeyEvent key) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            dispatchCallbackOrShowError();
        }
        return true;
    }

    @OnClick(R.id.cancel_button)
    void dismissOnClick() {
        dismiss();
    }

    @OnClick(R.id.confirm_button)
    void dispatchCallbackOrShowError() {
        String email = mEmailWrapper.getEditText().getText().toString();
        if (!ValidationUtil.isEmailValid(email)) {
            mEmailWrapper.setError(getString(R.string.error_invalid_email));
        } else {
            mEmailWrapper.setError(null);
            mListener.onResetPasswordRequest(email);
            dismiss();
        }
    }

    public interface Listener {

        void onResetPasswordRequest(String email);
    }
}
