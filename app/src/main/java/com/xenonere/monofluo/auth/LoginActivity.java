package com.xenonere.monofluo.auth;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.xenonere.monofluo.HomeActivity;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.util.DialogHelper;
import com.xenonere.monofluo.util.NetworkUtils;
import com.xenonere.monofluo.util.ValidationUtil;
import com.xenonere.monofluo.util.ViewHelper;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends BaseActivity implements ForgotPasswordFragment.Listener {

    //@format:off
    @Bind(R.id.password_editText) EditText mPasswordView;
    @Bind(R.id.username_editText) EditText mUsernameView;
    @Bind(R.id.progressBar) ProgressBar mProgressBar;
    //@format:on

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleAppBarConfiguration(R.drawable.ic_arrow_back_white_24dp);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }

    @OnTextChanged(R.id.username_editText)
    void usernameTextChanged(CharSequence text) {
        mUsernameView.setError(null);
    }

    @OnTextChanged(R.id.password_editText)
    void passwordTextChanged(CharSequence text) {
        mPasswordView.setError(null);
    }

    @OnClick(R.id.login_button)
    void attemptLogin() {
        final String username = mUsernameView.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean hasNetworkAccess = NetworkUtils
                .verifyNetworkAccess(this, getSupportFragmentManager());
        boolean isInputValid = validateInput(username, password);
        if (hasNetworkAccess && isInputValid) {
            onLoadingChange(true);
            ParseUser.logInInBackground(username, password, mLogInCallback);
        }
    }

    private boolean validateInput(String username, String password) {
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        boolean isValid = true;

        if (!ValidationUtil.isEmailValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            isValid = false;
        }

        if (!ValidationUtil.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onResetPasswordRequest(String email) {
        ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null && e.getCode() != ParseException.EMAIL_NOT_FOUND) {
                    DialogHelper.showError(e, LoginActivity.this, getSupportFragmentManager());
                } else {
                    DialogHelper.showSnackbarkMessage(LoginActivity.this,
                            findViewById(android.R.id.content), R.string.action_ok,
                            R.string.information_reset_email_sent, Snackbar.LENGTH_LONG, null);
                }
            }
        });
    }

    @OnClick(R.id.forgotPassword_textView)
    void onForgotClick() {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.show(getSupportFragmentManager(), ForgotPasswordFragment.TAG);
    }

    @Bind({R.id.login_button, R.id.password_editText, R.id.username_editText,
            R.id.forgotPassword_textView})
    List<TextView> mAllFormViews;

    private void onLoadingChange(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        ButterKnife.apply(mAllFormViews, ViewHelper.ENABLED, !isLoading);
    }

    @OnClick(R.id.skip_button)
    void skipAuth() {
        ParseUser.logInInBackground("ljubisa987@gmail.com", "123123", mLogInCallback);
    }

    private LogInCallback mLogInCallback = new LogInCallback() {
        @Override
        public void done(ParseUser parseUser, ParseException e) {
            if (isFinishing()) {
                return;
            }

            onLoadingChange(false);
            if (e == null) {
                ParseHelper.addUserToInstallation();
                launchInNewTask(HomeActivity.class);
            } else {
                if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                    DialogHelper.showError(R.string.error_invalid_credentials,
                            LoginActivity.this, getSupportFragmentManager());
                } else {
                    DialogHelper.showError(e, LoginActivity.this, getSupportFragmentManager());
                }
            }
        }
    };

}
