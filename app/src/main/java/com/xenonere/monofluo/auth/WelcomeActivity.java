package com.xenonere.monofluo.auth;

import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.MonofluoApp;
import com.xenonere.monofluo.R;

import android.os.Bundle;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity {

    @Bind(R.id.logo_textView) TextView mLogoView;
    @Bind(R.id.moto_textView) TextView mMotoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLogoView.setTypeface(MonofluoApp.getLogoFont());
        mMotoView.setTypeface(MonofluoApp.getMotoFont());
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_welcome;
    }

    @OnClick(R.id.login_button)
    void launchLogin() {
        launchInSameTask(LoginActivity.class);
    }

    @OnClick(R.id.register_button)
    void launchRegister() {
        launchInSameTask(RegisterActivity.class);
    }

}
