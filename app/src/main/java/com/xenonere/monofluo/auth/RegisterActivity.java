package com.xenonere.monofluo.auth;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.xenonere.monofluo.HomeActivity;
import com.xenonere.monofluo.R;
import com.xenonere.monofluo.common.BaseActivity;
import com.xenonere.monofluo.common.ParseHelper;
import com.xenonere.monofluo.util.DialogHelper;
import com.xenonere.monofluo.util.NetworkUtils;
import com.xenonere.monofluo.util.ValidationUtil;
import com.xenonere.monofluo.util.ViewHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    //@format:off
    @Bind(R.id.email_editText) EditText mEmailView;
    @Bind(R.id.name_editText) EditText mNameView;
    @Bind(R.id.password_editText) EditText mPasswordView;
    @Bind(R.id.progressBar) ProgressBar mProgressBar;
    //@format:on

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleAppBarConfiguration(R.drawable.ic_arrow_back_white_24dp);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_register;
    }

    @OnClick(R.id.register_button)
    void attemptRegistration() {
        final String name = mNameView.getText().toString().trim();
        final String email = mEmailView.getText().toString().trim();
        final String password = mPasswordView.getText().toString().trim();

        boolean hasNetworkAccess = NetworkUtils
                .verifyNetworkAccess(this, getSupportFragmentManager());
        boolean isInputValid = validateInput(name, email, password);

        if (hasNetworkAccess && isInputValid) {
            onLoadingChange(true);
            ParseUser user = new ParseUser();
            user.setEmail(email);
            user.setUsername(email);
            user.setPassword(password);
            user.put("name", name);
            user.signUpInBackground(mSignUpCallback);
        }
    }

    private final SignUpCallback mSignUpCallback = new SignUpCallback() {
        @Override
        public void done(ParseException e) {
            if (isFinishing()) return;

            if (e == null) {
                ParseHelper.addUserToInstallation();
                launchInNewTask(HomeActivity.class);
            } else {
                onLoadingChange(false);
                switch (e.getCode()) {
                    case ParseException.EMAIL_TAKEN:
                        mEmailView.setError(getString(R.string.error_email_taken));
                        mEmailView.requestFocus();
                        break;
                    default:
                        DialogHelper.showError(e, RegisterActivity.this,
                                getSupportFragmentManager());
                        break;
                }
            }
        }
    };

    boolean validateInput(String name, String email, String password){
        mNameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);

        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_name_invalid));
            isValid = false;
        }

        if (!ValidationUtil.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            isValid = false;
        }

        if (!ValidationUtil.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            isValid = false;
        }
        return isValid;
    }

    @Bind({ R.id.register_button, R.id.email_editText, R.id.password_editText, R.id.name_editText})
    List<TextView> mAllFormViews;

    private void onLoadingChange(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        ButterKnife.apply(mAllFormViews, ViewHelper.ENABLED, !isLoading);
    }


}
