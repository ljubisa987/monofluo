package com.xenonere.monofluo.util;

import android.test.AndroidTestCase;

import java.util.Calendar;

public class CalendarUtilTest extends AndroidTestCase {

    public void testTodayDoesReturnsToday(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 1);

        assertTrue(CalendarUtil.isToday(calendar));

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);

        assertTrue(CalendarUtil.isToday(calendar));
    }

    public void testTodayDoesNotReturnsToday(){
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.DAY_OF_MONTH, 1);

        assertFalse(CalendarUtil.isToday(calendar));

        calendar.roll(Calendar.DAY_OF_MONTH, -2);

        assertFalse(CalendarUtil.isToday(calendar));
    }

}
