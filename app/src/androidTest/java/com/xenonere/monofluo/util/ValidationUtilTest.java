package com.xenonere.monofluo.util;

import android.test.AndroidTestCase;

public class ValidationUtilTest extends AndroidTestCase {

    public void testValidEmail() {
        assertTrue(ValidationUtil.isEmailValid("ljubisa987@gmail.com"));
        assertTrue(ValidationUtil.isEmailValid("lj@g.ch"));
    }

    public void testInvalidEmail() {
        assertFalse(ValidationUtil.isEmailValid(".com"));
        assertFalse(ValidationUtil.isEmailValid("lj"));
    }

    public void testValidPassword() {
        assertTrue(ValidationUtil.isPasswordValid("123456"));
        assertTrue(ValidationUtil.isPasswordValid("morethan6chars"));
    }

    public void testInvalidPassword() {
        assertFalse(ValidationUtil.isEmailValid("12345"));
        assertFalse(ValidationUtil.isEmailValid("hello"));
    }


}
